<?php

namespace App\Form;

use App\Entity\User;
use App\Entity\Computer;
use App\Entity\Planning;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;



class PlanningType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('beginAt')
            ->add('endAt')
            ->add('computer', EntityType::class, [
                'class' => Computer::class, 
                'choice_label' => 'name'
            ] )
            ->add('user', EntityType::class, [
                'class' => User::class,
                'choice_label' => 'username'
            ] )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Planning::class,
        ]);
    }
}
