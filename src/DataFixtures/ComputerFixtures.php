<?php

namespace App\DataFixtures;

use App\Entity\Computer;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class ComputerFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for($i = 1; $i <= 10; $i++){
            $computer = new Computer();
            $computer->setName("Ordinateur n°$i")
                     ->setLocation($i);
                     
        
            $manager->persist($computer);
        }

        $manager->flush();
    }
}
