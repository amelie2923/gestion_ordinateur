<?php

namespace App\Controller;

use App\Entity\Computer;
use App\Repository\ComputerRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;


class ComputerController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function home()
    {
        return $this->render('security/login.html.twig');
    }
    
    /**
     * @Route("admin/computers", name="computers")
     */
    public function index(ComputerRepository $repo)
    {
        $computers = $repo->findAll();

        return $this->render('computers/index.html.twig', [
            'controller_name' => 'ComputerController',
            'computers' => $computers
        ]);
    }

    /**
     * @Route("/admin/computer/new", name="computer_create")
     * @Route("/admin/computer/{id}/edit", name="computer_edit")
     */
    public function form(Computer $computer = null, Request $request, EntityManagerInterface $manager) {
        if(!$computer){
            $computer = new Computer();
        }

        $form = $this->createFormBuilder($computer)
                     ->add('name')
                     ->add('location')
                     ->add('ip')
                     ->getForm();
                     
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $manager->persist($computer);
            $manager->flush();

            return $this->redirectToRoute('computer_show', ['id' => $computer->getID()]);
        }

        return $this->render('computers/create.html.twig', [
            'formComputer' => $form->createView(),
            'editMode' => $computer->getId() !== null
        ]);
    }

    /**
     * @Route("/admin/computer/{id}", name="computer_show")
     */
    public function show(Computer $computer){
        
        return $this->render('computers/show.html.twig', [
            'computer' => $computer
        ]);
    }

}
